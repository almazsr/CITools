#XPlat: .NET Standard, .NET Core, .NET Framework >=4.6 (Latest project format)
#FullFw: .NET Framework <=4.5 (Legacy project format)

function Main
{
	try {
		PrepareArtifactsDirectory
		BuildXPlat		
		AnalyzeXPlat		
		ConvertCodeQualityReportsXPlat
		TestXPlat
		GenerateCoverageReportXPlat
		Publish
	}
	catch {	
		exit 1
	}
}

function GetProjectDirectory
{
	if ($ENV:CI_PROJECT_DIR -eq $null) {
		(Resolve-Path "..").Path
	} else {
		"$ENV:CI_PROJECT_DIR"
	}
}
$projectDirectory=GetProjectDirectory

#Solutions and tests
$xPlatSolution="$projectDirectory\src\GitLabCIAdapters.sln"
$xPlatTestsDirectory="$projectDirectory\test\GitLabCIAdapters.Tests"

# Tools
$toolsDirectory="C:\tools"
$dotnetExe="C:\Program Files\dotnet\dotnet.exe"
$reportGeneratorExe="$toolsDirectory\reportgenerator\4.6.4\tools\net47\ReportGenerator.exe"
$openCoverExe="$toolsDirectory\opencover\4.7.922\tools\OpenCover.Console.exe"	
$nugetExe="$toolsDirectory\NuGet\nuget.exe"
$metricsExe="$toolsDirectory\Metrics\Metrics\Metrics.exe"
$inspectCodeExe="$toolsDirectory\InspectCode\inspectCode.exe"
$inspectCodeToCodeClimateExe="$toolsDirectory\ConvertInspectCodeToCodeClimate\ConvertInspectCodeToCodeClimate.exe"
$convertMicrosoftMetricsToOpenMetricsExe="$toolsDirectory\ConvertMicrosoftMetricsToOpenMetrics\ConvertMicrosoftMetricsToOpenMetrics.exe"

#Artifact paths
$artifactsDirectory="$projectDirectory\artifacts"
$coverageDirectory="$artifactsDirectory\coverage"
$codeQualityDirectory="$artifactsDirectory\codequality"
$coverageDirectoryXPlat="$coverageDirectory\xplat"
$coverageReportDirectory="$coverageDirectory\report"
$coverageOutputFullFw="$coverageDirectory\fullfw.coverage.xml"
$Global:coverageOutputXPlat=$null

function PrepareArtifactsDirectory
{
	"Prepare artifacts directory"
	if (Test-Path $artifactsDirectory) {
		Remove-Item $artifactsDirectory -Recurse
	}
	New-Item -ItemType directory $coverageDirectory | Out-Null
	New-Item -ItemType directory $codeQualityDirectory | Out-Null
}

function BuildXPlat
{
	"Build XPlat solution"	
	dotnet build $xPlatSolution
}

function Publish
{
	"Publish solution"	
	dotnet publish $xPlatSolution -p:PublishProfile=FolderProfile
}

function AnalyzeXPlat
{
	"Inspect XPlat code"
	& $inspectCodeExe $xPlatSolution -o="$codeQualityDirectory\XPlat.InspectCode.xml"
	"Calculate XPlat metrics"
	& $metricsExe /solution:$xPlatSolution /out:$codeQualityDirectory\XPlat.Metrics.xml
}

function BuildFullFw
{
	"Restore FullFw solution"
	& $nugetExe restore $fullFwSolution
	"Build FullFw solution"
	dotnet build $fullFwSolution
}

function AnalyzeFullFw
{
	"Inspect FullFw code"
	& $inspectCodeExe $fullFwSolution -o="$codeQualityDirectory\FullFw.InspectCode.xml"		
	"Calculate FullFw metrics"
	& $metricsExe /solution:$fullFwSolution /out:$codeQualityDirectory\FullFw.Metrics.xml
}

function ConvertCodeQualityReportsXPlat
{
	"Output XPlat Code inspections as CodeClimate artifact"
	& $inspectCodeToCodeClimateExe "$codeQualityDirectory\XPlat.InspectCode.xml" "$codeQualityDirectory\codequality.json" $projectDirectory
	"Output XPlat metrics reports as OpenMetrics format"	
	& $convertMicrosoftMetricsToOpenMetricsExe "$codeQualityDirectory\XPlat.Metrics.xml" "$codeQualityDirectory\metrics.txt"
}

function ConvertCodeQualityReportsFullFw
{
	"Output FullFw Code inspections as CodeClimate artifact"
	& $inspectCodeToCodeClimateExe "$codeQualityDirectory\FullFw.InspectCode.xml" "$codeQualityDirectory\codequality.json" $projectDirectory
	"Output FullFw metrics reports as OpenMetrics format"
	& $convertMicrosoftMetricsToOpenMetricsExe "$codeQualityDirectory\FullFw.Metrics.xml" "$codeQualityDirectory\metrics.txt"
}

function ConvertCodeQualityReports
{
	"Output Code inspections as CodeClimate artifact"
	& $inspectCodeToCodeClimateExe "$codeQualityDirectory\XPlat.InspectCode.xml;$codeQualityDirectory\FullFw.InspectCode.xml" "$codeQualityDirectory\codequality.json"	
	"Output metrics reports as OpenMetrics format"
	& $convertMicrosoftMetricsToOpenMetricsExe "$codeQualityDirectory\XPlat.Metrics.xml;$codeQualityDirectory\FullFw.Metrics.xml" "$codeQualityDirectory\metrics.txt"
}

function TestXPlat
{
	"Run and calculate cover of XPlat tests"
	dotnet test $xPlatTestsDirectory --collect:"XPlat Code Coverage" -r $coverageDirectoryXPlat
	Get-ChildItem $coverageDirectoryXPlat -Filter "*coverage.cobertura.xml" -Recurse | ForEach {$Global:coverageOutputXPlat=$Global:coverageOutputXPlat + "`"" + $_.Directory + "\" + $_ + "`" ";}
}

function TestFullFw
{
	"Restore FullFw tests solution"
	& $nugetExe restore $fullFwTestsSolution	
	"Build FullFw tests solution"	
	dotnet build $fullFwTestsSolution
	"Run and calculate cover of FullFw tests"	
	# Please ensure that you registered Profiler.dlls: see Usage.rtf/.pdf
	& $openCoverExe -register:user -targetdir:$projectDirectory -target:$dotnetExe -targetargs:"test $fullFwTestsDll" -output:"$coverageOutputFullFw" -filter:"+[*]* -[*.Tests*]*"
	return $coverageOutputFullFw
}

function GenerateCoverageReportXPlat
{
	"Generate coverage report"	
	& $reportGeneratorExe "-reports:$Global:coverageOutputXPlat" "-targetdir:$coverageReportDirectory" "-reporttypes:Html;JsonSummary"
	& type $coverageReportDirectory\Summary.json
}

function GenerateCoverageReportFullFw
{
	"Generate coverage report"	
	& $reportGeneratorExe "-reports:$Global:coverageOutputFullFw" "-targetdir:$coverageReportDirectory" "-reporttypes:Html;JsonSummary"
	& type $coverageReportDirectory\Summary.json
}

function GenerateCoverageReport
{
	"Generate coverage report"	
	& $reportGeneratorExe "-reports:$coverageOutputXPlat;$coverageOutputFullFw" "-targetdir:$coverageReportDirectory" "-reporttypes:Html;JsonSummary"
	& type $coverageReportDirectory\Summary.json
}

Main