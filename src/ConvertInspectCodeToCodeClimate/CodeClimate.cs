﻿using Newtonsoft.Json;

namespace ConvertInspectCodeToCodeClimate
{
    public class CodeClimate
    {
        public class Issue
        {
            [JsonProperty("description")]
            public string Description { get; set; }

            [JsonProperty("fingerprint")]
            public string Fingerprint { get; set; }

            [JsonProperty("location")]
            public Location Location { get; set; }
        }

        public class Location
        {
            [JsonProperty("path")]
            public string Path { get; set; }

            [JsonProperty("lines")]
            public Lines Lines { get; set; }
        }

        public class Lines
        {
            [JsonProperty("begin")]
            public long Begin { get; set; }
        }
    }
}
