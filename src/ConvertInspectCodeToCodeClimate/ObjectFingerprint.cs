﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace ConvertInspectCodeToCodeClimate
{
    public static class ObjectFingerprint
    {
        internal static string Calculate(object obj)
        {
            byte[] ObjectToByteArray(object objectToSerialize)
            {
                using (var stream = new MemoryStream())
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, objectToSerialize);
                    return stream.ToArray();
                }
            }
            var bytes = ObjectToByteArray(obj);
            using (var md5Hash = MD5.Create())
            {
                var hash = md5Hash.ComputeHash(bytes);
                return string.Join("", hash.Select(ch => ch.ToString("x2")));
            }
        }
    }
}
