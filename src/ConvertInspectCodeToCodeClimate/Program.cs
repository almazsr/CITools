﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Serialization;

[assembly:InternalsVisibleTo("GitLabCIAdapters.Tests")]

namespace ConvertInspectCodeToCodeClimate
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                throw new InvalidOperationException("3 arguments are required");
            }

            var inputFiles = args[0].Split(';').Select(s => new FileInfo(s)).ToArray();

            var outputFile = new FileInfo(args[1]);

            var repositoryDirectory = new DirectoryInfo(args[2]);

            var inspectCodeSerializer = new XmlSerializer(typeof(InspectCode.Report));

            InspectCode.Report ReadInput(FileInfo inputFile)
            {
                using (var inputFileStream = inputFile.OpenRead())
                {
                    return (InspectCode.Report)inspectCodeSerializer.Deserialize(inputFileStream);
                }
            }

            var inspectCodeReports = inputFiles.Select(ReadInput).ToArray();

            var inspectCodeIssues = inspectCodeReports.SelectMany(r => r.Issues.Project.SelectMany(p => p.Issue)).ToArray();

            string GetPieceOfCodeByIssue(InspectCode.Issue inspectCodeIssue)
            {
                var file = new FileInfo(inspectCodeIssue.File);

                const int initialStreamPosition = 3;

                var lineString = inspectCodeIssue.Line;
                if (!int.TryParse(lineString, out var lineNumber))
                {
                    lineNumber = 1;
                }
                using (var fileStream = file.OpenRead())
                {
                    using (var fileReader = new StreamReader(fileStream))
                    {
                        var offsetSplits = inspectCodeIssue.Offset.Split("-");
                        if (offsetSplits.Length == 0 || !int.TryParse(offsetSplits[0], out var offsetStart))
                        {
                            return fileReader.ReadLine();
                        }
                        if (offsetSplits.Length < 2 || !int.TryParse(offsetSplits[1], out var offsetEnd))
                        {
                            offsetEnd = offsetStart + 1;
                        }
                        if (offsetEnd > file.Length)
                        {
                            offsetEnd = (int)file.Length;
                        }

                        var length = offsetEnd - offsetStart;
                        fileStream.Seek(offsetStart + initialStreamPosition, SeekOrigin.Begin);
                        var buffer = new char[length];
                        fileReader.ReadBlock(buffer, 0, length);

                        return new string(buffer);
                    }
                }
            }

            Dictionary<string, List<CodeIssue>> BuildCodeIssuesByFingerprint(InspectCode.Issue[] inspectCodeIssues)
            {
                var codeIssuesByFingerprint = new Dictionary<string, List<CodeIssue>>();
                foreach (var inspectCodeIssue in inspectCodeIssues)
                {
                    var codeIssue = new CodeIssue
                    {
                        UniqueIssue = new CodeIssue.Unique
                        {
                            File = inspectCodeIssue.File,
                            Message = inspectCodeIssue.Message,
                            RelatedPiece = GetPieceOfCodeByIssue(inspectCodeIssue),
                            TypeId = inspectCodeIssue.TypeId
                        },
                        SourceIssue = inspectCodeIssue
                    };

                    var issueFingerprint = ObjectFingerprint.Calculate(codeIssue.UniqueIssue);
                    if (!codeIssuesByFingerprint.ContainsKey(issueFingerprint))
                    {
                        codeIssuesByFingerprint.Add(issueFingerprint, new List<CodeIssue>());
                    }
                    codeIssuesByFingerprint[issueFingerprint].Add(codeIssue);
                    codeIssue.UniqueIssue.SequenceNumber = codeIssuesByFingerprint[issueFingerprint].Count;
                }
                return codeIssuesByFingerprint;
            }

            CodeClimate.Issue ConvertIssue(CodeIssue codeIssue)
            {
                var inspectCodeIssue = codeIssue.SourceIssue;
                var filePath = inspectCodeIssue.File.Replace(repositoryDirectory.FullName, "");
                var linuxFilePath = filePath.Replace('\\', '/').TrimStart('/');
                var line = !string.IsNullOrEmpty(inspectCodeIssue.Line) ? long.Parse(inspectCodeIssue.Line) : 0;

                return new CodeClimate.Issue
                {
                    Description = $"{inspectCodeIssue.Message}: {codeIssue.UniqueIssue.RelatedPiece}",
                    Fingerprint = ObjectFingerprint.Calculate(codeIssue.UniqueIssue),
                    Location = new CodeClimate.Location
                    {
                        Path = linuxFilePath,
                        Lines = new CodeClimate.Lines
                        {
                            Begin = line
                        }
                    }
                };
            }

            var codeIssuesByFingerprint = BuildCodeIssuesByFingerprint(inspectCodeIssues);

            var codeClimateIssues = codeIssuesByFingerprint.SelectMany(i => i.Value).Select(ConvertIssue).ToArray();

            var codeClimateJson = JsonConvert.SerializeObject(codeClimateIssues);

            File.WriteAllText(outputFile.FullName, codeClimateJson);
        }
    }
}
