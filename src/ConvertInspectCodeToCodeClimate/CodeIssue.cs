﻿using System;

namespace ConvertInspectCodeToCodeClimate
{
    [Serializable]
    public class CodeIssue
    {
        [Serializable]
        public class Unique
        {
            public string TypeId { get; set; }

            public string File { get; set; }

            public string RelatedPiece { get; set; }

            public string Message { get; set; }

            public int SequenceNumber { get; set; }
        }

        public Unique UniqueIssue { get; set; }

        public InspectCode.Issue SourceIssue { get; set; }
    }
}
