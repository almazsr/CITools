﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace ConvertInspectCodeToCodeClimate
{
    public class InspectCode
    {
        [Serializable]
        [XmlRoot(ElementName = "InspectionScope")]
        public class InspectionScope
        {
            [XmlElement(ElementName = "Element")]
            public string Element { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "Information")]
        public class Information
        {
            [XmlElement(ElementName = "Solution")]
            public string Solution { get; set; }
            [XmlElement(ElementName = "InspectionScope")]
            public InspectionScope InspectionScope { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "IssueType")]
        public class IssueType
        {
            [XmlAttribute(AttributeName = "Id")]
            public string Id { get; set; }
            [XmlAttribute(AttributeName = "Category")]
            public string Category { get; set; }
            [XmlAttribute(AttributeName = "CategoryId")]
            public string CategoryId { get; set; }
            [XmlAttribute(AttributeName = "Description")]
            public string Description { get; set; }
            [XmlAttribute(AttributeName = "Severity")]
            public string Severity { get; set; }
            [XmlAttribute(AttributeName = "WikiUrl")]
            public string WikiUrl { get; set; }
            [XmlAttribute(AttributeName = "SubCategory")]
            public string SubCategory { get; set; }
            [XmlAttribute(AttributeName = "Global")]
            public string Global { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "IssueTypes")]
        public class IssueTypes
        {
            [XmlElement(ElementName = "IssueType")]
            public List<IssueType> IssueType { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "Issue")]
        public class Issue
        {
            [XmlAttribute(AttributeName = "TypeId")]
            public string TypeId { get; set; }
            [XmlAttribute(AttributeName = "File")]
            public string File { get; set; }
            [XmlAttribute(AttributeName = "Offset")]
            public string Offset { get; set; }
            [XmlAttribute(AttributeName = "Line")]
            public string Line { get; set; }
            [XmlAttribute(AttributeName = "Message")]
            public string Message { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "Project")]
        public class Project
        {
            [XmlElement(ElementName = "Issue")]
            public List<Issue> Issue { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "Issues")]
        public class Issues
        {
            [XmlElement(ElementName = "Project")]
            public List<Project> Project { get; set; }
        }

        [Serializable]
        [XmlRoot(ElementName = "Report")]
        public class Report
        {
            [XmlElement(ElementName = "Information")]
            public Information Information { get; set; }
            [XmlElement(ElementName = "IssueTypes")]
            public IssueTypes IssueTypes { get; set; }
            [XmlElement(ElementName = "Issues")]
            public Issues Issues { get; set; }
            [XmlAttribute(AttributeName = "ToolsVersion")]
            public string ToolsVersion { get; set; }
        }
    }
}
