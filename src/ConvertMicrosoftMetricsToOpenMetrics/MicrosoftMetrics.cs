﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ConvertMicrosoftMetricsToOpenMetrics
{
    public class MicrosoftMetrics
    {
        [XmlRoot(ElementName = "Metric")]
        public class Metric
        {
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "Metrics")]
        public class Metrics
        {
            [XmlElement(ElementName = "Metric")]
            public List<Metric> Metric { get; set; }
        }

        [XmlRoot(ElementName = "Method")]
        public class Method
        {
            [XmlElement(ElementName = "Metrics")]
            public Metrics Metrics { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
            [XmlAttribute(AttributeName = "File")]
            public string File { get; set; }
            [XmlAttribute(AttributeName = "Line")]
            public string Line { get; set; }
        }

        [XmlRoot(ElementName = "Members")]
        public class Members
        {
            [XmlElement(ElementName = "Method")]
            public List<Method> Method { get; set; }
            [XmlElement(ElementName = "Property")]
            public List<Property> Property { get; set; }
            [XmlElement(ElementName = "Field")]
            public List<Field> Field { get; set; }
            [XmlElement(ElementName = "Event")]
            public Event Event { get; set; }
        }

        [XmlRoot(ElementName = "NamedType")]
        public class NamedType
        {
            [XmlElement(ElementName = "Metrics")]
            public Metrics Metrics { get; set; }
            [XmlElement(ElementName = "Members")]
            public Members Members { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
        }

        [XmlRoot(ElementName = "Types")]
        public class Types
        {
            [XmlElement(ElementName = "NamedType")]
            public List<NamedType> NamedType { get; set; }
        }

        [XmlRoot(ElementName = "Namespace")]
        public class Namespace
        {
            [XmlElement(ElementName = "Metrics")]
            public Metrics Metrics { get; set; }
            [XmlElement(ElementName = "Types")]
            public Types Types { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
        }

        [XmlRoot(ElementName = "Namespaces")]
        public class Namespaces
        {
            [XmlElement(ElementName = "Namespace")]
            public Namespace Namespace { get; set; }
        }

        [XmlRoot(ElementName = "Assembly")]
        public class Assembly
        {
            [XmlElement(ElementName = "Metrics")]
            public Metrics Metrics { get; set; }
            [XmlElement(ElementName = "Namespaces")]
            public Namespaces Namespaces { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
        }

        [XmlRoot(ElementName = "Target")]
        public class Target
        {
            [XmlElement(ElementName = "Assembly")]
            public Assembly Assembly { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
        }

        [XmlRoot(ElementName = "Accessors")]
        public class Accessors
        {
            [XmlElement(ElementName = "Method")]
            public List<Method> Method { get; set; }
        }

        [XmlRoot(ElementName = "Property")]
        public class Property
        {
            [XmlElement(ElementName = "Metrics")]
            public Metrics Metrics { get; set; }
            [XmlElement(ElementName = "Accessors")]
            public Accessors Accessors { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
            [XmlAttribute(AttributeName = "File")]
            public string File { get; set; }
            [XmlAttribute(AttributeName = "Line")]
            public string Line { get; set; }
        }

        [XmlRoot(ElementName = "Field")]
        public class Field
        {
            [XmlElement(ElementName = "Metrics")]
            public Metrics Metrics { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
            [XmlAttribute(AttributeName = "File")]
            public string File { get; set; }
            [XmlAttribute(AttributeName = "Line")]
            public string Line { get; set; }
        }

        [XmlRoot(ElementName = "Event")]
        public class Event
        {
            [XmlElement(ElementName = "Metrics")]
            public Metrics Metrics { get; set; }
            [XmlElement(ElementName = "Accessors")]
            public Accessors Accessors { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
            [XmlAttribute(AttributeName = "File")]
            public string File { get; set; }
            [XmlAttribute(AttributeName = "Line")]
            public string Line { get; set; }
        }

        [XmlRoot(ElementName = "Targets")]
        public class Targets
        {
            [XmlElement(ElementName = "Target")]
            public List<Target> Target { get; set; }
        }

        [XmlRoot(ElementName = "CodeMetricsReport")]
        public class CodeMetricsReport
        {
            [XmlElement(ElementName = "Targets")]
            public Targets Targets { get; set; }
            [XmlAttribute(AttributeName = "Version")]
            public string Version { get; set; }
        }

    }

}
