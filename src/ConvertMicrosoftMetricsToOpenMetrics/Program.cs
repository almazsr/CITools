﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace ConvertMicrosoftMetricsToOpenMetrics
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                throw new InvalidOperationException("2 arguments are required");
            }

            var inputFiles = args[0].Split(';').Select(s => new FileInfo(s)).ToArray();

            var outputFile = new FileInfo(args[1]);

            var microsoftMetricsSerializer = new XmlSerializer(typeof(MicrosoftMetrics.CodeMetricsReport));

            MicrosoftMetrics.CodeMetricsReport ReadInput(FileInfo inputFile)
            {
                using (var inputFileStream = inputFile.OpenRead())
                {
                    return (MicrosoftMetrics.CodeMetricsReport)microsoftMetricsSerializer.Deserialize(inputFileStream);
                }
            }

            var microsoftMetricsReports = inputFiles.Select(ReadInput).ToArray();

            Dictionary<string, double> ConvertToOpenMetrics(MicrosoftMetrics.CodeMetricsReport[] reports)
            {
                var metrics = reports.
                    SelectMany(r => r.Targets.Target.
                        SelectMany(t => t.Assembly.Metrics.Metric
                            .Select(x => new { Assembly = t.Assembly.Name, Metric = x })));

                var executableLinesByAssembly = metrics
                    .GroupBy(m => m.Assembly)
                    .ToDictionary(m => m.Key, m =>
                    {
                        var executableLinesMetric = m.First(x => x.Metric.Name == "SourceLines");
                        return double.Parse(executableLinesMetric.Metric.Value);
                    });

                var executableLinesSum = executableLinesByAssembly.Sum(e => e.Value);

                var weightByAssembly = executableLinesByAssembly
                    .ToDictionary(e => e.Key, e => e.Value / executableLinesSum);

                return metrics.
                    GroupBy(m => m.Metric.Name).
                    ToDictionary(m => m.Key, m =>
                        m.Sum(x =>
                        {
                            var result = double.Parse(x.Metric.Value);
                            // If the metric is not volume then consider weight of each assembly
                            if (!m.Key.Contains("Lines"))
                            {
                                result *= weightByAssembly[x.Assembly];
                            }
                            return result;
                        }));
            }

            var openMetricsReport = ConvertToOpenMetrics(microsoftMetricsReports);

            using (var fileWriteStream = outputFile.OpenWrite())
            {
                using (var outputFileStreamWriter = new StreamWriter(fileWriteStream))
                {
                    foreach (var metric in openMetricsReport)
                    {
                        outputFileStreamWriter.WriteLine($"{metric.Key} {metric.Value:0.###}");
                    }
                }
            }
        }
    }
}
