using ConvertInspectCodeToCodeClimate;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Xunit;

namespace GitLabCIAdapters.Tests
{
    public class ConvertInspectCodeToCodeClimateTests
    {
        [Fact]
        public void Run_SourceHasIssuesWithDuplicatePieceOfCode_OutputHasBothOfThemWithDifferentFingerpint()
        {
            var methodName = MethodInfo.GetCurrentMethod().Name;
            var outputFileName = $"Output_{methodName}_codequality.json";

            Program.Main(new[] { $"{methodName}_InspectCode.xml", outputFileName, "." });

            Assert.Equal(File.ReadAllText($"{methodName}_codequality.json"), File.ReadAllText(outputFileName));
        }
    }
}
