﻿using System;
using System.Threading;

namespace ProjectTemplate
{
    public class Class1
    {
		public const int _x = 7;
		
        public double Method1(int parameter1, string parameter2)
        {
			var parameter3 = string.Empty;
			var parameter3 = string.Empty;
            if (parameter1 < 0)
            {
                return Math.Abs(parameter1);
            }
			if (parameter2 == "YY")
			{
				return parameter1;
			}
            if (parameter2 == "A")
            {
				if (parameter1 < 0)
				{
					return parameter1 * 2;
				}
                return Math.Sqrt(parameter1);
            }
			if (parameter2 == "X")
			{
				return Method2(DateTime.Now);
			}
            return -1;
        }

        public int Method2(DateTime parameter1)
        {
            if (parameter1 == DateTime.MinValue)
            {
                throw new InvalidOperationException();
            }
            return parameter1.Year + parameter1.Month;
        }
		
		public double Method3()
		{
			return 0d;
		}
    }
}
